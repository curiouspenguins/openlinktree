# OpenLinkTree

OpenLinkTree is the open-source alternative to LinkTr.ee where you host it yourself. It provides a basic template that looks just like a LinkTr.ee.

## Getting started

Copy the contents of links.html and edit to suit. When you're done upload links.html to wherever you would like to host your custom LinkTree.

See an example here: https://curiouspenguins.com/links.html</br>
Download here: https://olt.curiouspenguins.com</br>

## License
This is an open source project, feel free to use it how you like. CopyLeft. 

